package com.kenfogel.gopigo3controllerrest.service;

import com.kenfogel.gopigo3driver.GoPiGo3;
import java.io.IOException;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Ken Fogel
 */
@Path("gopigo3")
public class GoPiGo3Service {

    private static final byte MOTOR_LEFT = 1;
    private static final byte MOTOR_RIGHT = 2;

    @Context
    private UriInfo context;

    @Inject
    private GoPiGo3 gopigo;

    /**
     * Creates a new instance of GoPiGo3Service
     */
    public GoPiGo3Service() {
        super();
    }

    /**
     * GET method to turn on eyes
     *
     * @return
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("/on")
    public String turnOnEyes() throws IOException, InterruptedException {
        gopigo.doLeftEyeLED((byte) 100, (byte) 100, (byte) 100);
        gopigo.doRightEyeLED((byte) 100, (byte) 100, (byte) 100);
        return "<html> " + "<title>" + "GoPiGo3 Rest Controller" + "</title>"
                + "<body><h1>" + "Turn On Eyes" + "</body></h1>" + "</html> ";
    }

    /**
     * GET method to turn off eyes
     *
     * @return
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("/off")
    public String turnOffEyes() throws IOException, InterruptedException {
        gopigo.doLeftEyeLED((byte) 0, (byte) 0, (byte) 0);
        gopigo.doRightEyeLED((byte) 0, (byte) 0, (byte) 0);
        return "<html> " + "<title>" + "GoPiGo3 Rest Controller" + "</title>"
                + "<body><h1>" + "Turn Off Eyes" + "</body></h1>" + "</html> ";
    }

    /**
     * GET method to retrieve distance in mm from the ultrasonic sensor
     *
     * @return
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("/distance")
    public String getDistance() throws IOException, InterruptedException {
        int mm = gopigo.doUltra();
        return "<html> " + "<title>" + "GoPiGo3 Rest Controller" + "</title>"
                + "<body><h1>" + "Distance = " + mm + "</body></h1>" + "</html> ";
    }

    /**
     * GET method to move forward
     *
     * @return
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("/forward")
    public String goForward() throws IOException, InterruptedException {
        gopigo.runBothMotors((byte) 100);
        return "<html> " + "<title>" + "GoPiGo3 Rest Controller" + "</title>"
                + "<body><h1>" + "Going forward" + "</body></h1>" + "</html> ";
    }

    /**
     * GET method to move backwards
     *
     * @return
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("/backwards")
    public String goBackwards() throws IOException, InterruptedException {
        gopigo.runBothMotors((byte) -100);
        return "<html> " + "<title>" + "GoPiGo3 Rest Controller" + "</title>"
                + "<body><h1>" + "Going backwards" + "</body></h1>" + "</html> ";
    }

    /**
     * GET method to stop
     *
     * @return
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("/stop")
    public String goStop() throws IOException, InterruptedException {
        gopigo.stopMotors();
        return "<html> " + "<title>" + "GoPiGo3 Rest Controller" + "</title>"
                + "<body><h1>" + "Stopping" + "</body></h1>" + "</html> ";
    }

    /**
     * GET method to turn left
     *
     * @return
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("/left")
    public String goLeft() throws IOException, InterruptedException {
        gopigo.stopMotor(MOTOR_LEFT);
        gopigo.runMotor(MOTOR_RIGHT, (byte) 100);
        return "<html> " + "<title>" + "GoPiGo3 Rest Controller" + "</title>"
                + "<body><h1>" + "Turning Left" + "</body></h1>" + "</html> ";
    }

    /**
     * GET method to turn left
     *
     * @return
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    
    
    
    
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("/right")
    public String goRight() throws IOException, InterruptedException {
        gopigo.stopMotor(MOTOR_RIGHT);
        gopigo.runMotor(MOTOR_LEFT, (byte) 100);
        return "<html> " + "<title>" + "GoPiGo3 Rest Controller" + "</title>"
                + "<body><h1>" + "Turning Right" + "</body></h1>" + "</html> ";
    }
}
